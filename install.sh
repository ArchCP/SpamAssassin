#!/bin/bash

## Install the packages
yaourt -Syy --noconfirm $(cat ./packages.list);

## Copy the files
cp -Rlv ./etc/* /etc/;

## Update Spamassassin
sa-update;

## Compile the Spamassassin database
sa-compile;

## Update the permissions on the Razor directory
chown spamd:spamd /etc/mail/spamassassin/razor;


## Setup the Razor installation
su - spamd -s /bin/bash -c "razor-admin -home=/etc/mail/spamassassin/razor -register";
su - spamd -s /bin/bash -c "razor-admin -home=/etc/mail/spamassassin/razor -create";
su - spamd -s /bin/bash -c "razor-admin -home=/etc/mail/spamassassin/razor -discover";

## Enable the Spamassassin service
systemctl enable spamassassin.service;

## Start spamassassin
systemctl start spamassassin.service;

